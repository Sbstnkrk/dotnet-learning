using System;

namespace projekt__1
{
class Meow
    {
        public string Name;
        public int Age;

        public Meow(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public void Method()
        {
            Console.WriteLine("{0}{1}", Name, Age);
        }

        public void Method(string text)
        {
            Console.WriteLine("Hej " + text);
        }
        public int Method2()
        {
           return 23;
        }
    }   
}